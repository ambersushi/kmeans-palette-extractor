## About
A simple bash script to extract a color palette from an image using ImageMagick's `-kmeans` option.

Prints the hex codes for the colors as well as color samples using ANSI 24-bit color.

## Requirements
`convert`
`uuidgen`

## Usage
`./kmeansPaletteExtractor.sh <input_image> <number_of_colors_to_extract>`

Sample output:

```
$ ./kmeansPaletteExtractor.sh in.jpg 4
#ae2d2b     
#f66910     
#5c455c     
#371a25     
```
