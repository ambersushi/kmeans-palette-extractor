#!/bin/bash

# Checking for arguments.
if [[ $1 == "" ]] || [[ $2 == "" ]]
then
	echo "usage: $0 <input_image> <number_of_colors_to_extract>"

	exit 1
fi

# Checking if input image file exists.
if [ ! -e $1 ]
then
	echo "Input image \"$1\" not found!"

	exit 2
fi

# Generating a temp filename.
temp=$(uuidgen)

# Using imagemagick to get kmeans colors.
convert $1 -scale 256x256! -kmeans $2 -strip /tmp/$temp.ppm

# Printing the colors.
for i in $(xxd -s +15 -p -c 3 /tmp/$temp.ppm | sort -n | uniq | sort -n)
do
	r=$((16#${i:0:2}))
	g=$((16#${i:2:2}))
	b=$((16#${i:4:2}))

	printf "#%02x%02x%02x \033[48;2;%d;%d;%dm    \033[0m\n" $r $g $b $r $g $b
done

rm -f /tmp/$temp.ppm

exit 0
